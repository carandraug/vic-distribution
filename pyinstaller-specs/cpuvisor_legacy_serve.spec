#!/usr/bin/python

# PyInstaller spec file for cpuvisor_legacy_serve

block_cipher = None


a = Analysis(
    ["../install/bin/cpuvisor_legacy_serve.py"],
    pathex=["install/share/vgg-classifier/", "pyinstaller-specs/"],
    binaries=[],
    datas=[],
    hiddenimports=["gevent", "gevent.monkey", "pyclient"],
    hookspath=[],
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)
exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name="cpuvisor_legacy_serve",
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=True,
)
coll = COLLECT(
    exe,
    a.binaries,
    a.zipfiles,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name="cpuvisor_legacy_serve",
)
